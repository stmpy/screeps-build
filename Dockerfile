ARG NODE_VERSION=8.8.1

FROM node:${NODE_VERSION}-alpine

WORKDIR /app

RUN apk add --update --no-cache \
  git

RUN npm install -g grunt-cli

CMD ["npm"]
